# Prerequisites

## Ubuntu

First you must to install powerline usign the package manager, for ubuntu:
`sudo add-apt-repository universe`
and
`sudo apt install --yes powerline`

second clone the powerline repo
`git clone git@gitlab.com:nicoandresr/my-awesome-powerline.git ~/.config/powerline`

## Arch Linux

just install the powerline and powerline-fonts

```shell
sudo pacman -S powerline powerline-fonts
```

# Install

Clone this repo and create the syslink

`git clone git@gitlab.com:nicoandresr/tmux-workspaces.git ~/.config/tmux`

`ln -s .config/tmux/tmux.conf .tmux.conf`

# Mac Users
Install Victor mono in order to get ligatures
```sh
brew tap homebrew/cask-fonts
brew install --cask font-victor-mono-nerd-font
```
